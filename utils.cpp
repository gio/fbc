#include "utils.h"

#include <cryptopp/sha.h>
#include <iomanip>
#include <sstream>

namespace gio::fbc {

std::string hex_digest(const std::string& s) {
    using namespace std;
    CryptoPP::SHA256 sha256;
    sha256.Update(reinterpret_cast<const ::byte*>(s.data()), s.size());
    std::string bin_digest(sha256.DigestSize(), '\0');
    sha256.Final(reinterpret_cast<::byte*>(bin_digest.data()));
    ostringstream ret;
    for (const auto &c : bin_digest) {
        ret << std::hex << setfill('0') << setw(2) << nouppercase << (unsigned int)(unsigned char)c;
    }
    return ret.str();
}

}
