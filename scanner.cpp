#include "scanner.h"

#include <iostream>

#include "utils.h"
#include "parsing.h"
#include "pictures.h"

namespace gio::fbc {

const static std::set<boost::filesystem::path> CPP_EXTS{{".cpp"}, {".cxx"}, {".cc"}, {".C"}, {".hpp"}, {".hxx"}, {".H"}, {".h"},
                                                        {".ipp"}, {".cu"}, {".cl"}, {".mm"}, {".inl"}, {".doc"}, {".inc"}};
const static std::set<boost::filesystem::path> C_EXTS{{".c"}, {".m"}, {".gci"}};
const static std::set<boost::filesystem::path> HASH_EXTS{{".sh"}, {".py"}, {".jam"}, {".pl"}, {".mak"}, {".idx"}, {".cmake"}, {".dox"},
                                                         {".pro"}, {".pri"}, {".tcl"}, {".yml"}, {".toml"}};
const static std::set<boost::filesystem::path> HASH_NAMES{{"Jamfile"}, {"Jamfile.v2"}, {"jamfile"}, {"jamfile.v2"}, {"Jamroot"},
                                                          {"CMakeLists.txt"}, {"Makefile"}, {"Jambase"}, {"Doxyfile.in"}};
const static std::set<boost::filesystem::path> SEMICOLON_EXTS{{".asm"}, {".mc"}, {".cmd"}};
const static std::set<boost::filesystem::path> PERCENT_EXTS{{".tex"}};
const static std::set<boost::filesystem::path> BANG_EXTS{{".com"}};
const static std::set<boost::filesystem::path> DOUBLE_SLASH_EXTS{{".qml"}, {".d"}, {".flex"}};
const static std::set<boost::filesystem::path> BAT_EXTS{{".bat"}};
const static std::set<boost::filesystem::path> M4_EXTS{{".m4"}};
const static std::set<boost::filesystem::path> ADOC_EXTS{{".adoc"}};
const static std::set<boost::filesystem::path> RB_EXTS{{".rb"}};
const static std::set<boost::filesystem::path> QUICKBOOK_EXTS{{".qbk"}, {".quickbook"}};
const static std::set<boost::filesystem::path> HTML_EXTS{{".html"}, {".htm"}, {".xml"}, {".gold"}, {".gold-html"}, {".md"},
                                                         {".xsl"}, {".svg"}, {".mml"}, {".dtd"}, {".gold2"}, {".sgml"}, {".docbook"},
                                                         {".ui"}, {".graffle"}, {".xsd"}, {".dtdxml"}, {"ent"}, {".toyxml"}, {".qrc"}};
const static std::set<boost::filesystem::path> C_LIKE_EXTS{{".css"}, {".S"}, {".yy"}, {".y"}, {".sass"}, {".scss"}, {".erb"}};
const static std::set<boost::filesystem::path> REST_EXTS{{".rst"}};
const static std::set<boost::filesystem::path> EXIF_EXTS{{".jpg"}, {".jpeg"}, {".CRW"}};
const static std::set<boost::filesystem::path> PNG_EXTS{{".png"}, {".PNG"}, {".png_original"}};
const static std::set<boost::filesystem::path> TXT_EXTS{{".txt"}};

bool is_file_binary(const boost::filesystem::path &p) {
    // Try to decode the file as UTF-8; if it fails, assume it is binary
    boost::filesystem::wifstream fin(p);
    fin.imbue(std::locale("en_US.UTF-8"));
    while (fin.good()) {
        wchar_t c;
        fin >> c;
    }
    return !fin.eof();
}

void path_rec(global_result &gr, const boost::filesystem::path &p, const config &conf) {
    using namespace std;
    using namespace boost::filesystem;
    if (is_directory(p)) {
        for (const auto &entry : directory_iterator(p)) {
            path_rec(gr, entry, conf);
        }
    } else if (is_regular_file(p)) {
        vector<pair<size_t, string>> comments;
        std::string comment_processing;
        const auto &p_str = p.string();
        gr.scanned.push_back(p);
        for (const auto &re : conf.ignore_path_regex) {
            if (boost::regex_match(p_str.begin(), p_str.end(), re)) {
                gr.ignored.push_back(p);
                return;
            }
        }
        if (file_size(p) == 0) {
            gr.empty.push_back(p);
            return;
        } else if (in(p.extension(), CPP_EXTS)) {
            boost::filesystem::ifstream fin(p);
            try {
                comments = find_c_comments(fin, true);
                comment_processing = "cpp comments";
            } catch (boost::wave::cpplexer::lexing_exception&) {
                fin.seekg(0, ios_base::beg);
                comments = find_delimited_comments(fin, "/*", "*/");
                comment_processing = "manual cpp comments";
            }
        } else if (in(p.extension(), C_EXTS)) {
            boost::filesystem::ifstream fin(p);
            try {
                comments = find_c_comments(fin, false);
                comment_processing = "c comments";
            } catch (boost::wave::cpplexer::lexing_exception&) {
                fin.seekg(0, ios_base::beg);
                comments = find_delimited_comments(fin, "/*", "*/");
                comment_processing = "manual c comments";
            }
        } else if (in(p.extension(), HASH_EXTS) || in(p.filename(), HASH_NAMES)) {
            boost::filesystem::ifstream fin(p);
            comments = find_line_comments(fin, "#");
            comment_processing = "hash comments";
        } else if (in(p.extension(), SEMICOLON_EXTS)) {
            boost::filesystem::ifstream fin(p);
            comments = find_line_comments(fin, ";");
            comment_processing = "semicolon comments";
        } else if (in(p.extension(), PERCENT_EXTS)) {
            boost::filesystem::ifstream fin(p);
            comments = find_line_comments(fin, "%");
            comment_processing = "percent comments";
        } else if (in(p.extension(), BANG_EXTS)) {
            boost::filesystem::ifstream fin(p);
            comments = find_line_comments(fin, "!");
            comment_processing = "bang comments";
        } else if (in(p.extension(), DOUBLE_SLASH_EXTS)) {
            boost::filesystem::ifstream fin(p);
            comments = find_line_comments(fin, "//");
            comment_processing = "double slash comments";
        } else if (in(p.extension(), BAT_EXTS)) {
            boost::filesystem::ifstream fin(p);
            comments = find_line_comments(fin, "::");
            comment_processing = "bat comments";
        } else if (in(p.extension(), M4_EXTS)) {
            boost::filesystem::ifstream fin(p);
            comments = find_line_comments(fin, "dnl");
            comment_processing = "m4 comments";
        } else if (in(p.extension(), ADOC_EXTS)) {
            boost::filesystem::ifstream fin(p);
            comments = find_delimited_comments(fin, "////", "////");
            comment_processing = "adoc comments";
        } else if (in(p.extension(), RB_EXTS)) {
            boost::filesystem::ifstream fin(p);
            comments = find_delimited_comments(fin, "=begin", "=end");
            comment_processing = "rb comments";
        } else if (in(p.extension(), QUICKBOOK_EXTS)) {
            boost::filesystem::ifstream fin(p);
            comments = find_delimited_comments(fin, "[/", "]");
            comment_processing = "quickbook comments";
        } else if (in(p.extension(), HTML_EXTS)) {
            boost::filesystem::ifstream fin(p);
            comments = find_delimited_comments(fin, "<!--", "-->");
            comment_processing = "html comments";
        } else if (in(p.extension(), C_LIKE_EXTS)) {
            boost::filesystem::ifstream fin(p);
            comments = find_delimited_comments(fin, "/*", "*/");
            comment_processing = "c like comments";
        } else if (in(p.extension(), REST_EXTS)) {
            boost::filesystem::ifstream fin(p);
            comments = find_rst_comments(fin);
            comment_processing = "rst comments";
        } else if (in(p.extension(), EXIF_EXTS)) {
            comments = read_exif_tags(p.string());
            comment_processing = "exif";
        } else if (in(p.extension(), PNG_EXTS)) {
            comments = read_png_metadata(p.string());
            comment_processing = "png";
        } else if (in(p.extension(), TXT_EXTS)) {
            boost::filesystem::ifstream fin(p);
            std::vector<std::pair<size_t, std::string>> comments;
            if (comments.empty()) {
                fin.seekg(0);
                comments = find_line_comments(fin, "#");
                comment_processing = "detected hash comments";
            }
            if (comments.empty()) {
                fin.seekg(0);
                comments = find_delimited_comments(fin, "<!--", "-->");
                comment_processing = "detected html comments";
            }
            if (comments.empty()) {
                fin.seekg(0);
                comments = find_line_comments(fin, "//");
                comment_processing = "detected double slash comments";
            }
            if (comments.empty()) {
                fin.seekg(0);
                comments = file_as_comments(fin);
                comment_processing = "fallback whole file";
            }
        } else {
            //gr.unknown.push_back(p);
            //return;
            boost::filesystem::ifstream fin(p);
            comments = file_as_comments(fin);
            comment_processing = "whole file";
        }
        if (comments.empty()) {
            gr.empty.push_back(p);
            return;
        }
        auto fr = find_license(comments, conf);
        fr.binary = is_file_binary(p);
        fr.comment_processing = comment_processing;
        auto status = fr.status();
        if (status == file_result::status_t::good) {
            gr.good.push_back(make_tuple(p, comments, fr));
            for (const auto &license : fr.licenses) {
                gr.licenses[license].insert(p);
            }
            for (const auto &copy : fr.copyright) {
                gr.copyright_holders[copy.second].insert(copy.first.begin(), copy.first.end());
                gr.authors[copy.second].insert(make_pair(p, fr.hash));
            }
        } else if (status == file_result::status_t::useless) {
            gr.useless.push_back(p);
        } else if (status == file_result::status_t::bad) {
            gr.bad.push_back(make_tuple(p, comments, fr));
            /*cout << p << '\n';
            print_comments(comments);
            fr.dump(cout);
            cout << '\n';*/
        } else {
            throw std::runtime_error("should not arrive here");
        }
    } else {
        throw std::runtime_error(std::string("unknown file type: ") + p.string());
    }
}

}
