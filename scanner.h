#pragma once

#include <vector>
#include <map>
#include <string>
#include <set>

#include <boost/filesystem.hpp>

#include "license.h"
#include "config.h"

namespace gio::fbc {

struct global_result {
    std::vector<boost::filesystem::path> scanned;
    std::vector<std::tuple<boost::filesystem::path, std::vector<std::pair<std::size_t, std::string>>, file_result>> good;
    std::vector<std::tuple<boost::filesystem::path, std::vector<std::pair<std::size_t, std::string>>, file_result>> bad;
    std::vector<boost::filesystem::path> useless;
    std::vector<boost::filesystem::path> empty;
    std::vector<boost::filesystem::path> unknown;
    std::vector<boost::filesystem::path> ignored;
    std::map<std::string, std::set<unsigned long>> copyright_holders;
    std::map<std::string, std::set<std::pair<boost::filesystem::path, std::string>>> authors;
    std::map<std::string, std::set<boost::filesystem::path>> licenses;
};

void path_rec(global_result &gr, const boost::filesystem::path &p, const config &conf);

}
