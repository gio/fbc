#include "config.h"

#include <iostream>

#include <yaml-cpp/yaml.h>

namespace gio::fbc {

config read_config() {
    YAML::Node source = YAML::LoadFile("/home/giovanni/.fbc.yaml");
    config ret;
    for (const auto &hash : source["pre_ignore_regex"]) {
        ret.pre_ignore_regex.push_back(boost::regex(hash.as<std::string>(), boost::regex::perl | boost::regex::icase));
    }
    for (const auto &hash : source["ignore_regex"]) {
        ret.ignore_regex.push_back(boost::regex(hash.as<std::string>(), boost::regex::perl | boost::regex::icase));
    }
    for (const auto &hash : source["ignore_path_regex"]) {
        ret.ignore_path_regex.push_back(boost::regex(hash.as<std::string>(), boost::regex::perl | boost::regex::icase));
    }
    for (const auto &hash : source["force_hashes"]) {
        ret.force_hashes.insert(hash.as<std::string>());
    }
    for (const auto &hash: source["override_hashes"]) {
        std::vector<std::pair<std::set<unsigned long>, std::string>> copyright;
        for (const auto &copy : hash.second) {
            std::set<unsigned long> years;
            for (const auto &year : copy[0]) {
                years.insert(year.as<unsigned long>());
            }
            copyright.push_back(std::make_pair(std::move(years), copy[1].as<std::string>()));
        }
        ret.override_hashes.insert(std::make_pair(hash.first.as<std::string>(), copyright));
    }
    for (const auto &rename_item : source["rename_authors"]) {
        auto &rename_author = ret.rename_authors[rename_item.first.as<std::string>()];
        for (const auto &rename_to : rename_item.second) {
            rename_author.insert(rename_to.as<std::string>());
        }
    }
    for (const auto &license_item : source["licenses"]) {
        auto &license = ret.licenses[license_item.first.as<std::string>()];
        for (const auto &text : license_item.second) {
            license.push_back(text.as<std::string>());
        }
    }
    return ret;
}

}
