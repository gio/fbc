#include <iostream>
#include <vector>
#include <string>
#include <utility>
#include <cassert>
#include <fstream>
#include <algorithm>
#include <random>

#include <boost/filesystem.hpp>

#include <giolib/main.h>

#include "years.h"
#include "parsing.h"
#include "utils.h"
#include "license.h"
#include "scanner.h"
#include "config.h"
#include "meta.h"
#include "debian.h"
#include "data.h"

namespace gio::fbc {

void do_report(const data_t &data, std::ostream &fout) {
    using namespace std;
    const auto &gr = data.gr;
    mt19937 me;
    fout << "Scanned: " << gr.scanned.size() << '\n';
    fout << "Good: " << gr.good.size() << '\n';
    fout << "Bad: " << gr.bad.size() << '\n';
    fout << "Useless: " << gr.useless.size() << '\n';
    fout << "Empty: " << gr.empty.size() << '\n';
    fout << "Unknown: " << gr.unknown.size() << '\n';
    fout << "Ignored: " << gr.ignored.size() << '\n';
    fout << "All copyright holders:\n";
    for (const auto &holder : gr.copyright_holders) {
        fout << " * " << holder.first << ": " << render_years(holder.second) << '\n';
    }
    /*fout << "Good files:\n";
    for (const auto &good : gr.good) {
        fout << " * " << get<0>(good) << '\n';
    }*/
    fout << "Bad files:\n";
    for (const auto &bad : gr.bad) {
        fout << " * " << get<0>(bad) << '\n';
    }
    fout << "Empty files:\n";
    for (const auto &empty : gr.empty) {
        fout << " * " << empty << '\n';
    }
    fout << "Useless files:\n";
    for (const auto &useless : gr.useless) {
        fout << " * " << useless << '\n';
    }
    fout << "Unknown files:\n";
    for (const auto &unknown : gr.unknown) {
        fout << " * " << unknown << '\n';
    }
    fout << "Ignored files:\n";
    for (const auto &ignored : gr.ignored) {
        fout << " * " << ignored << '\n';
    }
}

void do_bads(const data_t &data, std::ostream &fout) {
    using namespace std;
    const auto &gr = data.gr;
    mt19937 me;
    auto bad = gr.bad;
    shuffle(bad.begin(), bad.end(), me);
    for (size_t i = 0; i < std::min<size_t>(20, bad.size()); i++) {
        const auto &fr = bad.at(i);
        fout << get<0>(fr) << '\n';
        if (!get<2>(fr).binary) {
            fout << "Comments:\n";
            print_comments(get<1>(fr), fout);
        }
        get<2>(fr).dump(fout);
        fout << '\n';
    }
}

void do_summary(const data_t &data, std::ostream &fout) {
    using namespace std;
    const auto &gr = data.gr;
    if (gr.bad.size() > 0) {
        fout << data.base_dir << ": " << gr.scanned.size() << ' ' << gr.good.size() << ' ' << gr.bad.size() << ' ' << gr.useless.size() << ' ' << gr.empty.size() << ' ' << gr.unknown.size() << ' ' << gr.ignored.size() << '\n';
    }
}

void do_list_unknown(const data_t &data, std::ostream &fout) {
    using namespace std;
    for (const auto &unknown : data.gr.unknown) {
        fout << unknown.string() << '\n';
    }
}

void do_authors(const data_t &data, std::ostream &fout) {
    using namespace std;
    for (const auto &author : data.gr.authors) {
        fout << author.first << ":\n";
        size_t num = 0;
        for (const auto &p : author.second) {
            if (num == 10) {
                fout << "   [and " << author.second.size() - num << " others]\n";
                break;
            }
            fout << " * " << p.first.string() << " (\"" << p.second << "\")\n";
            num++;
        }
    }
}

void do_metas(const data_t &data, std::ostream &fout) {
    using namespace std;
    for (const auto &item : data.mr.authors) {
        if (item.first.find('/') == string::npos) {
            //continue;
        }
        fout << item.first << ":\n";
        for (const auto &author : item.second) {
            fout << " * " << author << '\n';
        }
    }
}

void do_debian(const data_t &data, std::ostream &fout) {
    gen_debian_copyright(data, fout);
}

void do_licenses(const data_t &data, std::ostream &fout) {
    using namespace std;
    for (const auto &license : data.gr.licenses) {
        fout << license.first << ":\n";
        size_t num = 0;
        for (const auto &p : license.second) {
            if (num == 10) {
                fout << "   [and " << license.second.size() - num << " others]\n";
                break;
            }
            fout << " * " << p.string() << '\n';
            num++;
        }
    }
}

std::pair<std::ostream*, std::unique_ptr<std::ostream>> open_output_file(const std::string &path) {
    if (path == "-") {
        return std::make_pair(&std::cout, nullptr);
    } else {
        auto fout = std::make_unique<std::ofstream>(path);
        auto ptr = fout.get();
        return std::make_pair(ptr, std::move(fout));
    }
}

int main(const std::string &base_dir_str = fire::arg({"--base-dir", "base directory"}, "."),
         const fire::optional<std::string> &report_filename = fire::arg({"--report", "report filename"}),
         const fire::optional<std::string> &bads_filename = fire::arg({"--bads", "bads filename"}),
         const fire::optional<std::string> &summary_filename = fire::arg({"--summary", "summary filename"}),
         const fire::optional<std::string> &list_unknown_filename = fire::arg({"--list-unknown", "unknown list filename"}),
         const fire::optional<std::string> &authors_filename = fire::arg({"--authors", "authors filename"}),
         const fire::optional<std::string> &metas_filename = fire::arg({"--metas", "metas filename"}),
         const fire::optional<std::string> &debian_filename = fire::arg({"--debian", "debian filename"}),
         const fire::optional<std::string> &licenses_filename = fire::arg({"--licenses", "licenses filename"})) {
    bool compute_gr = false;
    if (report_filename.has_value() || bads_filename.has_value() || summary_filename.has_value()
            || list_unknown_filename.has_value() || authors_filename.has_value() || debian_filename.has_value()
            || licenses_filename.has_value()) {
        compute_gr = true;
    }
    data_t data(base_dir_str, compute_gr);

    if (report_filename.has_value()) {
        auto fout = open_output_file(report_filename.value());
        do_report(data, *fout.first);
    }

    if (bads_filename.has_value()) {
        auto fout = open_output_file(bads_filename.value());
        do_bads(data, *fout.first);
    }

    if (summary_filename.has_value()) {
        auto fout = open_output_file(summary_filename.value());
        do_summary(data, *fout.first);
    }

    if (list_unknown_filename.has_value()) {
        auto fout = open_output_file(list_unknown_filename.value());
        do_list_unknown(data, *fout.first);
    }

    if (authors_filename.has_value()) {
        auto fout = open_output_file(authors_filename.value());
        do_authors(data, *fout.first);
    }

    if (metas_filename.has_value()) {
        auto fout = open_output_file(metas_filename.value());
        do_metas(data, *fout.first);
    }

    if (debian_filename.has_value()) {
        auto fout = open_output_file(debian_filename.value());
        do_debian(data, *fout.first);
    }

    if (licenses_filename.has_value()) {
        auto fout = open_output_file(licenses_filename.value());
        do_licenses(data, *fout.first);
    }

    return 0;
}

int main2(int argc, char *argv[]) {
    using namespace std;
    if (argc != 2) {
        cerr << "Specify filename\n";
        return 1;
    }

    ifstream fin(argv[1]);
    const auto &comments = find_c_comments(fin, true);
    print_comments(comments, cout);
    find_license(comments, {}).dump(cout);

    return 0;
}

int main3(int argc, char *argv[]) {
    (void) argc;
    (void) argv;

    using namespace std;
    boost::regex re("(?:(?:\\(c\\)\\W+)?Copyright(?:\\W+\\(c\\))?|©)\\W+([-0-9, ]+[0-9])\\W+([^0-9][^\n]+[^.\n ])\\.?", boost::regex::perl | boost::regex::icase);
    std::string corpus = "© 2012,2014 Advanced Micro Devices, Inc. All rights reserved.";
    auto res = boost::regex_search(corpus, re);
    cout << boolalpha << res << endl;
    return 0;
}

}

int main(int argc, char *argv[]) {
    init_and_run(argc, const_cast<const char**>(argv), gio::fbc::main, false);
    return gio::fbc::main();
}
