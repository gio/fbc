#pragma once

#include <vector>
#include <utility>
#include <string>

namespace gio::fbc {

std::vector<std::pair<std::size_t, std::string>> read_exif_tags(const std::string &filename);
std::vector<std::pair<std::size_t, std::string>> read_png_metadata(const std::string &filename);

}
