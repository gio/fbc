#pragma once

#include <string>

namespace gio::fbc {

std::string hex_digest(const std::string& s);

template<typename T, typename Cont>
bool in(const T &t, const Cont &cont) {
    return cont.find(t) != cont.end();
}

}
