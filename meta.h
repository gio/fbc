#pragma once

#include <boost/filesystem.hpp>

#include "config.h"

namespace gio::fbc {

struct meta_results {
    std::vector<std::pair<std::string, std::set<std::string>>> authors;
};

void meta_path_rec(meta_results &mr, const boost::filesystem::path &p, const config &conf);

}
