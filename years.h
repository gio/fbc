#pragma once

#include <set>
#include <string>

namespace gio::fbc {

std::set<unsigned long> parse_years(const std::string &str);
std::string render_years(const std::set<unsigned long> &years);

}
