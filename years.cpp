#include "years.h"

#include <boost/regex.hpp>

const boost::regex SINGLE_YEAR_RE("([0-9]+)", boost::regex::perl | boost::regex::icase);
const boost::regex YEARS_RANGE_RE("([0-9]+) *-+ *([0-9]+)", boost::regex::perl | boost::regex::icase);
const boost::regex LIST_RE("(.*[^ ]) *, *([^ ].*)", boost::regex::perl | boost::regex::icase);
const unsigned int MIN_YEAR = 1980;
const unsigned int MAX_YEAR = 2020;

namespace gio::fbc {

std::set<unsigned long> parse_years(const std::string &str) {
    if (str == "--NO_NUMBER--") {
        return {};
    }
    boost::smatch mr;
    if (boost::regex_match(str, mr, SINGLE_YEAR_RE)) {
        std::string year_str(mr[1].first, mr[1].second);
        auto year = stoul(year_str);
        if (year < MIN_YEAR || year > MAX_YEAR) {
            throw std::invalid_argument("invalid");
        }
        return {year};
    } else if (boost::regex_match(str, mr, YEARS_RANGE_RE)) {
        auto from = stoul(std::string(mr[1].first, mr[1].second));
        auto to = stoul(std::string(mr[2].first, mr[2].second));
        if (from < MIN_YEAR || from > MAX_YEAR) {
            throw std::invalid_argument("invalid");
        }
        if (to < 10) {
            //std::cerr << "Interpreting " << from << '-' << to << " as ";
            to = to + from / 10 * 10;
            //std::cerr << from << '-' << to << '\n';
        }
        if (to < 100) {
            //std::cerr << "Interpreting " << from << '-' << to << " as ";
            to = to + from / 100 * 100;
            //std::cerr << from << '-' << to << '\n';
        }
        if (to < MIN_YEAR || to > MAX_YEAR) {
            throw std::invalid_argument("invalid");
        }
        if (to < from) {
            throw std::invalid_argument("invalid");
        }
        std::set<unsigned long> ret;
        for (; from <= to; from++) {
            ret.insert(from);
        }
        return ret;
    } else if (boost::regex_match(str, mr, LIST_RE)) {
        std::string str1(mr[1].first, mr[1].second);
        std::string str2(mr[2].first, mr[2].second);
        auto ret1 = parse_years(str1);
        auto ret2 = parse_years(str2);
        std::copy(ret2.begin(), ret2.end(), std::inserter(ret1, ret1.end()));
        return ret1;
    } else {
        throw std::invalid_argument("invalid");
    }
}

std::string render_years(const std::set<unsigned long> &years) {
    if (years.empty()) {
        return "(no info)";
    }
    std::ostringstream oss;
    bool first = true;
    unsigned long from = 0;
    unsigned long to = 0;
    for (const auto &year : years) {
        if (from == 0) {
            from = year;
            to = year;
        } else if (year == to+1) {
            to = year;
        } else {
            if (!first) {
                oss << ", ";
            }
            first = false;
            if (to != from) {
                oss << from << '-' << to;
            } else {
                oss << from;
            }
            from = year;
            to = year;
        }
    }
    if (!first) {
        oss << ", ";
    }
    if (to != from) {
        oss << from << '-' << to;
    } else {
        oss << from;
    }
    return oss.str();
}

}
