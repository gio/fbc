#pragma once

#include <set>
#include <string>
#include <map>
#include <vector>
#include <utility>

#include <boost/regex.hpp>

namespace gio::fbc {

struct config {
    std::vector<boost::regex> pre_ignore_regex;
    std::vector<boost::regex> ignore_regex;
    std::vector<boost::regex> ignore_path_regex;
    std::set<std::string> force_hashes;
    std::map<std::string, std::vector<std::pair<std::set<unsigned long>, std::string>>> override_hashes;
    std::map<std::string, std::set<std::string>> rename_authors;
    std::map<std::string, std::vector<std::string>> licenses;
};

config read_config();

}
