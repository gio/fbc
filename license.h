#pragma once

#include <vector>
#include <utility>
#include <string>
#include <set>

#include "config.h"

namespace gio::fbc {

struct file_result {
    std::vector<std::pair<std::string, std::string>> copyright_raw;
    std::vector<std::pair<std::set<unsigned long>, std::string>> copyright;
    std::string hash;
    std::string leftover;
    std::set<std::string> licenses;
    std::string comment_processing;
    bool valid;
    bool bsl_link;
    bool years_valid;
    bool force;
    bool useless;
    bool binary;

    enum class status_t {
        bad,
        useless,
        good,
    };

    file_result();
    void dump(std::ostream &os) const;
    void process_copyright(const config &conf);
    status_t status() const;
};

file_result find_license(const std::vector<std::pair<std::size_t, std::string>> &comments, const config &conf);

}
