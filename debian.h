#pragma once

#include <boost/filesystem.hpp>

#include "data.h"

namespace gio::fbc {

void gen_debian_copyright(const data_t &data, std::ostream &fout);

}
