#include "license.h"

#include <iostream>

#include <boost/regex.hpp>

#include "years.h"
#include "utils.h"
#include "names.h"

namespace gio::fbc {

file_result::file_result() : valid(false), bsl_link(false), years_valid(true), force(false), useless(false) {}

void file_result::dump(std::ostream &os) const {
    os << (valid ? "Valid" : "Not valid") << ", " << (bsl_link ? "BSL link found" : "BSL link not found");
    os << ", " << (years_valid ? "Years valid" : "Years not valid");
    os << ", licenses:";
    for (const auto &license : this->licenses) {
        os << ' ' << license;
    }
    os << '\n';
    os << "Comment processing: " << comment_processing << "\n";
    os << "Hash: - \"" << hash << "\"\n";
    for (const auto &c : this->copyright) {
        os << "Years: " << render_years(c.first) << ", copyright holder: " << c.second << '\n';
    }
    if (!this->binary) {
        os << "Leftover:\n /-------\n";
        bool line_begin = true;
        for (const auto &c : this->leftover) {
            if (c == '\n') {
                if (!line_begin) {
                    os << '\n';
                }
                line_begin = true;
            } else {
                if (line_begin) {
                    os << " | ";
                }
                os << c;
                line_begin = false;
            }
        }
        if (!line_begin) {
            os << '\n';
        }
        os << " \\-------\n";
    } else {
        os << "Leftover: BINARY\n";
    }
}

void file_result::process_copyright(const config &conf) {
    for (const auto &copy : copyright_raw) {
        try {
            for (const auto &name : process_author_renaming(copy.second, conf)) {
                copyright.push_back(make_pair(parse_years(copy.first), name));
            }
        } catch (std::invalid_argument&) {
            //std::cout << "Error with " << copy.first << '\n';
            this->years_valid = false;
        } catch (std::out_of_range&) {
            //std::cout << "Error with " << copy.first << '\n';
            this->years_valid = false;
        }
    }
}

file_result::status_t file_result::status() const {
    if (this->useless) {
        return status_t::useless;
    }
    if (this->force || (this->valid && !this->licenses.empty() /*&& this->bsl_link*/ && !this->copyright.empty() && this->years_valid)) {
        return status_t::good;
    }
    return status_t::bad;
}

static std::string mangle_license(const std::string &s) {
    std::string ret;
    bool white = false;
    bool first_white = true;
    for (const auto &c : s) {
        if (c == ' ' || c == '\n' || c == '*') {
            white = true;
        } else {
            if (white && !first_white) {
                ret.append("\\W+");
            }
            white = false;
            first_white = false;
            if (c == '(' || c == ')' || c == '.') {
                ret.push_back('\\');
                ret.push_back(c);
            } else {
                ret.push_back(c);
            }
        }
    }
    return ret;
}

const auto regexps_base = std::vector<std::pair<boost::regex, std::function<void(file_result&, boost::smatch&)>>>{
    {boost::regex("(?:it\\W+is\\W+made\\W+available\\W+under|distributed\\W+under|distribution\\W+under|licensed\\W+under|(?:distribution\\W+is\\W+)?subject\\W+to|Use\\W+modification\\W+and\\W+distribution\\W+(?:of\\W+this\\W+Scalable\\W+Vector\\W+Graphic\\W+file\\W+)?(?:is|are)\\W+subject\\W+to)(\\W+the)?[^\"[:word:]]+Boost\\W+Software\\W+License\\W+Version\\W+1\\.0\\.?", boost::regex::perl | boost::regex::icase),
     [](file_result &fr, boost::smatch &mr){ (void) mr; fr.licenses.insert("BSL-1.0"); }},
    {boost::regex("License: Boost Software License, Version 1\\.0\\.", boost::regex::perl | boost::regex::icase),
     [](file_result &fr, boost::smatch &mr){ (void) mr; fr.licenses.insert("BSL-1.0"); }},
    {boost::regex("\\\\license\\W+Boost Software License 1\\.0", boost::regex::perl | boost::regex::icase),
     [](file_result &fr, boost::smatch &mr){ (void) mr; fr.licenses.insert("BSL-1.0"); }},
    {boost::regex("\\(?See(?:\\W+the)?\\W+accompany\\W*ing\\W+file\\W+(?:LICENSE_1_0\\.txt|LICENSE\\.md)\\W+or(?:\\W+a)?\\W+copy\\W+at\\W+<?http://(?:www\\.)?boost\\.org/LICENSE_1_0.txt>?\\.?\\)?", boost::regex::perl | boost::regex::icase),
     [](file_result &fr, boost::smatch &mr){ (void) mr; fr.bsl_link = true; }},
    {boost::regex("\\(?See\\W+http://www\\.boost\\.org/LICENSE_1_0.txt\\)?", boost::regex::perl | boost::regex::icase),
     [](file_result &fr, boost::smatch &mr){ (void) mr; fr.bsl_link = true; }},
    // Name <email>, year
    {boost::regex("(?:(?:\\(c\\)\\W+)?Copyright(?:\\W+\\(c\\))?|©)\\W+([^0-9()]+[^, ])\\W+<[^>\n]+>,\\W+([-0-9, ]+)\\.?", boost::regex::perl | boost::regex::icase),
     [](file_result &fr, boost::smatch &mr){ fr.copyright_raw.push_back(std::make_pair(std::string(mr[2].first, mr[2].second), std::string(mr[1].first, mr[1].second))); }},
    // Year then name
    {boost::regex("(?:(?:\\(c\\)\\W+)?Copyright(?:\\W+\\(c\\))?|©)\\W+([-0-9, ]+[0-9])(?:\\W+\\(c\\)|\\W+by)?\\W+([^0-9][^\n]+[^.\n ])\\.?", boost::regex::perl | boost::regex::icase),
     [](file_result &fr, boost::smatch &mr){ fr.copyright_raw.push_back(std::make_pair(std::string(mr[1].first, mr[1].second), std::string(mr[2].first, mr[2].second))); }},
    // Name then year
    {boost::regex("(?:(?:\\(c\\)\\W+)?Copyright(?:\\W+\\(c\\))?|©)\\W+([^0-9]+[^, ])\\W+([0-9][-0-9, ]+[0-9])\\.?", boost::regex::perl | boost::regex::icase),
     [](file_result &fr, boost::smatch &mr){ fr.copyright_raw.push_back(std::make_pair(std::string(mr[2].first, mr[2].second), std::string(mr[1].first, mr[1].second))); }},
    // Just name
    {boost::regex("(?:(?:\\(c\\)\\W+)?Copyright(?:\\W+\\(c\\))?|©)\\W+([^0-9\n]+[^.\n ])\\.?", boost::regex::perl | boost::regex::icase),
     [](file_result &fr, boost::smatch &mr){ fr.copyright_raw.push_back(std::make_pair(std::string("--NO_NUMBER--"), std::string(mr[1].first, mr[1].second))); }},
    // Special cases for Boost.Outcome
    {boost::regex("\\(c\\)\\W+([-0-9, ]+[0-9])\\W+([^0-9,][^\n,]+[^.,\n ])\\W+\\([0-9]+ commits?\\),\\W+([^0-9,][^\n,]+[^.,\n ])\\W+\\([0-9]+ commits?\\),\\W+([^0-9,][^\n,]+[^.,\n ])\\W+\\([0-9]+ commits?\\)\\W+and\\W+([^0-9,][^\n,]+[^.,\n ])\\W+\\([0-9]+ commits?\\)", boost::regex::perl | boost::regex::icase),
    [](file_result &fr, boost::smatch &mr){ fr.copyright_raw.push_back(std::make_pair(std::string(mr[1].first, mr[1].second), std::string(mr[2].first, mr[2].second)));
                                            fr.copyright_raw.push_back(std::make_pair(std::string(mr[1].first, mr[1].second), std::string(mr[3].first, mr[3].second)));
                                            fr.copyright_raw.push_back(std::make_pair(std::string(mr[1].first, mr[1].second), std::string(mr[4].first, mr[4].second)));
                                            fr.copyright_raw.push_back(std::make_pair(std::string(mr[1].first, mr[1].second), std::string(mr[5].first, mr[5].second))); }},
    {boost::regex("\\(c\\)\\W+([-0-9, ]+[0-9])\\W+([^0-9,][^\n,]+[^.,\n ])\\W+\\([0-9]+ commits?\\),\\W+([^0-9,][^\n,]+[^.,\n ])\\W+\\([0-9]+ commits?\\)\\W+and\\W+([^0-9,][^\n,]+[^.,\n ])\\W+\\([0-9]+ commits?\\)", boost::regex::perl | boost::regex::icase),
    [](file_result &fr, boost::smatch &mr){ fr.copyright_raw.push_back(std::make_pair(std::string(mr[1].first, mr[1].second), std::string(mr[2].first, mr[2].second)));
                                            fr.copyright_raw.push_back(std::make_pair(std::string(mr[1].first, mr[1].second), std::string(mr[3].first, mr[3].second)));
                                            fr.copyright_raw.push_back(std::make_pair(std::string(mr[1].first, mr[1].second), std::string(mr[4].first, mr[4].second))); }},
    {boost::regex("\\(c\\)\\W+([-0-9, ]+[0-9])\\W+([^0-9,][^\n,]+[^.,\n ])\\W+\\([0-9]+ commits?\\)\\W+and\\W+([^0-9,][^\n,]+[^.,\n ])\\W+\\([0-9]+ commits?\\)", boost::regex::perl | boost::regex::icase),
    [](file_result &fr, boost::smatch &mr){ fr.copyright_raw.push_back(std::make_pair(std::string(mr[1].first, mr[1].second), std::string(mr[2].first, mr[2].second)));
                                            fr.copyright_raw.push_back(std::make_pair(std::string(mr[1].first, mr[1].second), std::string(mr[3].first, mr[3].second))); }},
    {boost::regex("\\(c\\)\\W+([-0-9, ]+[0-9])\\W+([^0-9,][^\n,]+[^.,\n ])\\W+\\([0-9]+ commits?\\)", boost::regex::perl | boost::regex::icase),
    [](file_result &fr, boost::smatch &mr){ fr.copyright_raw.push_back(std::make_pair(std::string(mr[1].first, mr[1].second), std::string(mr[2].first, mr[2].second))); }},
};

static decltype(auto) compute_regexps(const config &conf) {
    using namespace std;
    std::vector<std::pair<boost::regex, std::function<void(file_result&, boost::smatch&)>>> ret;
    for (const auto &license : conf.licenses) {
        const auto &name = license.first;
        for (const auto &text : license.second) {
            ret.push_back(make_pair(boost::regex(mangle_license(text), boost::regex::perl | boost::regex::icase),
                                    [name](file_result &fr, boost::smatch &mr){ (void) mr; fr.licenses.insert(name); }));
        }
    }
    std::copy(regexps_base.begin(), regexps_base.end(), std::back_inserter(ret));
    return ret;
}

const auto nonempty_regexps = std::vector<boost::regex>{
    {boost::regex("\\(c\\)", boost::regex::perl | boost::regex::icase),},
    {boost::regex("copyright", boost::regex::perl | boost::regex::icase),},
    {boost::regex("license", boost::regex::perl | boost::regex::icase),},
    {boost::regex("bsl", boost::regex::perl | boost::regex::icase),},
    {boost::regex("rights", boost::regex::perl | boost::regex::icase),},
    //{boost::regex("Boost", boost::regex::perl | boost::regex::icase),},
};

const boost::regex white_re("\\W*", boost::regex::perl);

void find_license_rec(std::set<std::tuple<std::string::const_iterator, std::string::const_iterator, bool>> &res,
                      file_result &fr,
                      std::string::const_iterator begin,
                      std::string::const_iterator end,
                      const config &conf,
                      const std::vector<std::pair<boost::regex, std::function<void(file_result&, boost::smatch&)>>> &regexps) {
    for (const auto &re : conf.pre_ignore_regex) {
        boost::smatch mr;
        if (boost::regex_search(begin, end, mr, re)) {
            res.insert(std::make_tuple(mr[0].first, mr[0].second, false));
            find_license_rec(res, fr, begin, mr[0].first, conf, regexps);
            find_license_rec(res, fr, mr[0].second, end, conf, regexps);
            return;
        }
    }
    for (const auto &re : regexps) {
        boost::smatch mr;
        if (boost::regex_search(begin, end, mr, re.first)) {
            re.second(fr, mr);
            res.insert(std::make_tuple(mr[0].first, mr[0].second, true));
            //std::cout << "Matched: " << std::string(mr[0].first, mr[0].second) << '\n';
            find_license_rec(res, fr, begin, mr[0].first, conf, regexps);
            find_license_rec(res, fr, mr[0].second, end, conf, regexps);
            return;
        }
    }
    for (const auto &re : conf.ignore_regex) {
        boost::smatch mr;
        if (boost::regex_search(begin, end, mr, re)) {
            res.insert(std::make_tuple(mr[0].first, mr[0].second, false));
            find_license_rec(res, fr, begin, mr[0].first, conf, regexps);
            find_license_rec(res, fr, mr[0].second, end, conf, regexps);
            return;
        }
    }
}

file_result find_license(const std::vector<std::pair<std::size_t, std::string>> &comments, const config &conf) {
    using namespace std;
    ostringstream oss;
    vector<pair<size_t, pair<size_t, size_t>>> lines;
    for (const auto &c : comments) {
        size_t begin = oss.str().size();
        oss << c.second;
        size_t end = oss.str().size();
        oss << '\n';
        lines.push_back(make_pair(c.first, make_pair(begin, end)));
    }
    string comment = oss.str();
    file_result fr;
    fr.hash = hex_digest(comment);
    fr.force = conf.force_hashes.find(fr.hash) != conf.force_hashes.end();
    bool useless = true;
    for (const auto &re : nonempty_regexps) {
        if (boost::regex_search(comment.begin(), comment.end(), re)) {
            useless = false;
            break;
        }
    }
    if (useless) {
        fr.useless = true;
        return fr;
    }
    set<tuple<string::const_iterator, string::const_iterator, bool>> res;
    const auto &regexps = compute_regexps(conf);
    find_license_rec(res, fr, comment.begin(), comment.end(), conf, regexps);
    auto override_it = conf.override_hashes.find(fr.hash);
    if (res.empty()) {
        if (override_it != conf.override_hashes.end()) {
            fr.copyright = override_it->second;
            fr.force = true;
        }
        return fr;
    }
    //cout << "Found:\n";
    size_t begin = SIZE_MAX, end = 0;
    for (const auto &r : res) {
        size_t first = std::get<0>(r)-comment.begin();
        size_t second = std::get<1>(r)-comment.begin();
        if (std::get<2>(r)) {
            begin = std::min<size_t>(begin, first);
            end = std::max<size_t>(end, second);
        }
        //cout << first << "-" << second << ": ";
        //copy(r.first, r.second, ostream_iterator<char>(cout));
        fill(comment.begin()+first, comment.begin()+second, ' ');
        //cout << '\n';
    }
    boost::smatch mr;
    if (boost::regex_match(comment.cbegin()+begin, comment.cbegin()+end, mr, white_re)) {
        //cout << "Entire block was consumed\n";
        fr.valid = true;
    } else {
        /*cout << "Some text left over: ";
        copy(comment.cbegin()+begin, comment.cbegin()+end, ostream_iterator<char>(cout));
        cout << endl;*/
        fr.leftover = std::string(comment.cbegin()+begin, comment.cbegin()+end);
    }
    fr.process_copyright(conf);
    if (override_it != conf.override_hashes.end()) {
        fr.copyright = override_it->second;
        fr.force = true;
    }
    return fr;
}

}
