#pragma once

#include <vector>
#include <utility>
#include <string>
#include <iostream>

#include <boost/wave/cpplexer/cpplexer_exceptions.hpp>

namespace gio::fbc {

std::vector<std::pair<std::size_t, std::string>> find_line_comments(std::istream &fin, const std::string &com);
std::vector<std::pair<std::size_t, std::string>> find_delimited_comments(std::istream &fin, const std::string &start, const std::string &stop);
std::vector<std::pair<std::size_t, std::string>> find_rst_comments(std::istream &fin);
std::vector<std::string> split(const std::string &s, char c);
std::vector<std::pair<std::size_t, std::string>> find_c_comments(std::istream &fin, bool cpp);
std::vector<std::pair<std::size_t, std::string>> file_as_comments(std::istream &fin);
void print_comments(const std::vector<std::pair<std::size_t, std::string>> &comments, std::ostream &fout);

}
