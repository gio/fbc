#include "pictures.h"

#include <stdexcept>
#include <memory>
#include <cstring>
#include <iostream>
#include <functional>
#include <cerrno>

#include <libexif/exif-data.h>
#include <libexif/exif-entry.h>

#include <libpng/png.h>

namespace gio::fbc {

const std::vector<std::pair<ExifIfd, ExifTag>> TAGS = {
    {EXIF_IFD_0, EXIF_TAG_IMAGE_WIDTH},
    {EXIF_IFD_0, EXIF_TAG_COPYRIGHT},
    {EXIF_IFD_0, EXIF_TAG_ARTIST},
    {EXIF_IFD_0, EXIF_TAG_XP_AUTHOR},
    {EXIF_IFD_0, EXIF_TAG_IMAGE_DESCRIPTION},
    {EXIF_IFD_0, EXIF_TAG_SOFTWARE},
    {EXIF_IFD_EXIF, EXIF_TAG_USER_COMMENT},
};

std::vector<std::pair<std::size_t, std::string>> read_exif_tags(const std::string &filename) {
    using namespace std;
    auto data = std::unique_ptr<ExifData, void(*)(ExifData*)>(exif_data_new_from_file(filename.c_str()), exif_data_unref);
    if (!data) {
        return {};
    }
    vector<pair<size_t, string>> ret;
    for (const auto &tag_data : TAGS) {
        auto entry = exif_content_get_entry(data->ifd[tag_data.first], tag_data.second);
        if (!entry) {
            continue;
        }
        std::vector<char> buf(1024);
        while (true) {
            exif_entry_get_value(entry, buf.data(), buf.size());
            if (strlen(buf.data()) >= buf.size()-1) {
                buf.resize(2 * buf.size());
            } else {
                break;
            }
        }
        std::string line;
        for (const auto &c : buf) {
            if (c == '\r') {
                continue;
            } else if (c == '\n') {
                ret.push_back(std::make_pair(ret.size(), std::move(line)));
                line.clear();
            } else {
                line.push_back(c);
            }
        }
        ret.push_back(std::make_pair(ret.size(), std::move(line)));
    }
    for (auto &line : ret) {
        size_t i;
        for (i = 0; i < line.second.size() && isspace(line.second[i]); i++);
        if (i == line.second.size()) {
            line.second = "";
        } else {
            size_t j;
            for (j = line.second.size()-1; isspace(line.second[j]); j--);
            line.second = line.second.substr(i, j+1-i);
        }
    }
    return ret;
}

void fclose_wrapper(FILE *fd) {
    int res = fclose(fd);
    if (res != 0) {
        throw std::runtime_error("cannot close file: " + std::to_string(errno));
    }
}

struct png_exception : public std::runtime_error {
    using std::runtime_error::runtime_error;
};

[[noreturn]] void user_error_fn(png_structp png_ptr,
                   png_const_charp error_msg) {
    (void) png_ptr;
    throw png_exception(error_msg);
}

void user_warning_fn(png_structp png_ptr,
                     png_const_charp warning_msg) {
    (void) png_ptr;
    (void) warning_msg;
}

std::vector<std::pair<std::size_t, std::string>> read_png_metadata(const std::string &filename) {
    using namespace std;
    try {
        auto fp = std::unique_ptr<FILE, void(*)(FILE*)>(fopen(filename.c_str(), "rb"), fclose_wrapper);
        unsigned char header[8];
        size_t header_read = fread(header, 1, sizeof(header), fp.get());
        if (header_read == 0) {
            return {};
        }
        bool is_png = !png_sig_cmp(header, 0, header_read);
        if (!is_png) {
            return {};
        }
        png_infop info_ptr = nullptr;
        png_infop end_info = nullptr;
        auto png_ptr = std::unique_ptr<png_struct, std::function<void(png_structp)>>(png_create_read_struct(PNG_LIBPNG_VER_STRING, nullptr, user_error_fn, user_warning_fn),
                                                                                [&info_ptr,&end_info](png_structp x){ png_destroy_read_struct(&x, &info_ptr, &end_info); });
        if (!png_ptr) {
            return {};
        }
        info_ptr = png_create_info_struct(png_ptr.get());
        if (!info_ptr) {
            return {};
        }
        end_info = png_create_info_struct(png_ptr.get());
        if (!end_info) {
            return {};
        }
        png_init_io(png_ptr.get(), fp.get());
        png_set_sig_bytes(png_ptr.get(), header_read);
        png_read_info(png_ptr.get(), info_ptr);
        vector<pair<size_t, string>> ret;
        png_textp text_ptr;
        int num_text = 0;
        if (png_get_text(png_ptr.get(), info_ptr, &text_ptr, &num_text) > 0) {
            for (int i = 0; i < num_text; i++) {
                const auto key = std::string(text_ptr[i].key);
                const auto value = std::string(text_ptr[i].text);
                //std::cerr << key << ": " << value << '\n';
                if (key != "Copyright") {
                    continue;
                }
                std::string line;
                for (const auto &c : value) {
                    if (c == '\r') {
                        continue;
                    } else if (c == '\n') {
                        ret.push_back(std::make_pair(ret.size(), std::move(line)));
                        line.clear();
                    } else {
                        line.push_back(c);
                    }
                }
                ret.push_back(std::make_pair(ret.size(), std::move(line)));
            }
        }
        for (auto &line : ret) {
            size_t i;
            for (i = 0; i < line.second.size() && isspace(line.second[i]); i++);
            if (i == line.second.size()) {
                line.second = "";
            } else {
                size_t j;
                for (j = line.second.size()-1; isspace(line.second[j]); j--);
                line.second = line.second.substr(i, j+1-i);
            }
        }
        return ret;
    } catch (png_exception &e) {
        //std::cerr << "Failing because: " << e.what() << '\n';
        return {};
    }
}

}
