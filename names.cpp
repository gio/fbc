#include "names.h"

#include <iostream>

#include <boost/algorithm/string.hpp>
#include <boost/regex.h>

static const boost::regex NAME_EMAIL_RE("(.*) <.*>");

namespace gio::fbc {

std::vector<std::string> split_author_name(const std::string &name) {
    using namespace std;
    vector<string> ret;
    string::size_type pos = 0;
    while (true) {
        auto comma_pos = name.find(",", pos);
        auto ampersand_pos = name.find("&", pos);
        auto and_pos = name.find(" and ", pos);
        if (comma_pos < ampersand_pos && comma_pos < and_pos && comma_pos != string::npos) {
            ret.push_back(name.substr(pos, comma_pos-pos));
            pos = comma_pos + 1;
        } else if (ampersand_pos < and_pos && ampersand_pos != string::npos) {
            ret.push_back(name.substr(pos, ampersand_pos-pos));
            pos = ampersand_pos + 1;
        } else if (and_pos != string::npos) {
            ret.push_back(name.substr(pos, and_pos-pos));
            pos = and_pos + 5;
        } else {
            ret.push_back(name.substr(pos));
            break;
        }
    }
    for_each(ret.begin(), ret.end(), [](auto &x) {
        boost::algorithm::trim(x);boost::smatch sm;
        bool res = boost::regex_match(x, sm, NAME_EMAIL_RE);
        if (res) {
            x = string(sm[1].first, sm[1].second);
        }
    });
    ret.erase(remove_if(ret.begin(), ret.end(), [](const auto &x) { return x == ""; }), ret.end());
    if (false && ret.size() != 1) {
        cerr << "Splitting \"" << name << "\" to";
        for (const auto &name : ret) {
            cerr << " \"" << name << "\",";
        }
        cerr << '\n';
    }
    return ret;
}

std::vector<std::string> process_author_renaming(const std::string &name, const config &conf) {
    using namespace std;
    vector<string> ret;
    auto it = conf.rename_authors.find(name);
    if (it == conf.rename_authors.end()) {
        auto splitted = split_author_name(name);
        for (const auto &splitted_name : splitted) {
            auto it2 = conf.rename_authors.find(splitted_name);
            if (it2 == conf.rename_authors.end()) {
                ret.push_back(splitted_name);
            } else {
                for (const auto &real_name : it2->second) {
                    ret.push_back(real_name);
                }
            }
        }
    } else {
        for (const auto &real_name : it->second) {
            ret.push_back(real_name);
        }
    }
    return ret;
}

}
