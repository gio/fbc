#include "parsing.h"

#include <boost/wave/cpplexer/cpp_lex_interface.hpp>
#include <boost/wave/cpplexer/cpp_lex_token.hpp>
#include <boost/wave/cpplexer/cpp_lex_iterator.hpp>
#include <boost/wave/cpplexer/cpp_lex_interface_generator.hpp>
#include <boost/wave/cpplexer/cpplexer_exceptions.hpp>

namespace gio::fbc {

std::vector<std::pair<std::size_t, std::string>> find_line_comments(std::istream &fin, const std::string &com) {
    using namespace std;
    vector<pair<size_t, string>> ret;
    size_t line_num = 0;
    for (string line; getline(fin, line);) {
        auto pos = line.find(com);
        if (pos != string::npos) {
            ret.push_back(make_pair(line_num, line.substr(pos + com.size())));
        }
        line_num++;
    }
    for (auto &line : ret) {
        size_t i;
        for (i = 0; i < line.second.size() && (line.second[i] == com[0] || isspace(line.second[i])); i++);
        if (i == line.second.size()) {
            line.second = "";
        } else {
            size_t j;
            for (j = line.second.size()-1; line.second[j] == com[0] || isspace(line.second[j]); j--);
            line.second = line.second.substr(i, j+1-i);
        }
    }
    return ret;
}

std::vector<std::pair<std::size_t, std::string>> find_delimited_comments(std::istream &fin, const std::string &start, const std::string &stop) {
    using namespace std;
    vector<pair<size_t, string>> ret;
    size_t line_num = 0;
    bool in_comment = false;
    for (string line; getline(fin, line);) {
        string comm_line;
        while (!line.empty()) {
            if (in_comment) {
                auto pos = line.find(stop);
                if (pos != string::npos) {
                    comm_line += line.substr(0, pos);
                    line = line.substr(pos + stop.size());
                    in_comment = false;
                } else {
                    comm_line += line;
                    line = "";
                }
            } else {
                auto pos = line.find(start);
                if (pos != string::npos) {
                    line = line.substr(pos + start.size());
                    in_comment = true;
                } else {
                    line = "";
                }
            }
        }
        if (!comm_line.empty()) {
            ret.push_back(make_pair(line_num, comm_line));
        }
        line_num++;
    }
    for (auto &line : ret) {
        size_t i;
        for (i = 0; i < line.second.size() && isspace(line.second[i]); i++);
        if (i == line.second.size()) {
            line.second = "";
        } else {
            size_t j;
            for (j = line.second.size()-1; isspace(line.second[j]); j--);
            line.second = line.second.substr(i, j+1-i);
        }
    }
    return ret;
}

// Not a good RST parser, but hopefully good enough for us
std::vector<std::pair<std::size_t, std::string>> find_rst_comments(std::istream &fin) {
    using namespace std;
    const std::string STARTER = ".. copyright::";
    vector<pair<size_t, string>> ret;
    size_t line_num = 0;
    bool in_comment = false;
    for (string line; getline(fin, line);) {
        if (in_comment) {
            if (line == "") {
                in_comment = false;
            } else {
                ret.push_back(make_pair(line_num, line));
            }
        } else {
            auto pos = line.find(STARTER);
            if (pos != string::npos) {
                ret.push_back(make_pair(line_num, line.substr(pos + STARTER.size())));
                in_comment = true;
            }
        }
        line_num++;
    }
    for (auto &line : ret) {
        size_t i;
        for (i = 0; i < line.second.size() && isspace(line.second[i]); i++);
        if (i == line.second.size()) {
            line.second = "";
        } else {
            size_t j;
            for (j = line.second.size()-1; isspace(line.second[j]); j--);
            line.second = line.second.substr(i, j+1-i);
        }
    }
    return ret;
}

std::vector<std::string> split(const std::string &s, char c) {
    using namespace std;
    vector<string> ret;
    size_t p = 0;
    while (true) {
        size_t q = s.find(c, p);
        if (q == string::npos) {
            ret.push_back(s.substr(p));
            break;
        } else {
            ret.push_back(s.substr(p, q-p));
            p = q+1;
        }
    }
    return ret;
}

std::vector<std::pair<std::size_t, std::string>> find_c_comments(std::istream &fin, bool cpp) {
    using namespace std;
    using namespace boost::wave;
    vector<pair<size_t, string>> ret;
    string input(std::istreambuf_iterator<char>(fin.rdbuf()), std::istreambuf_iterator<char>());
    typedef cpplexer::lex_iterator<boost::wave::cpplexer::lex_token<>> lex_iterator_type;

    language_support lang = language_support::support_option_preserve_comments;
    if (cpp) {
        lang = language_support(lang | language_support::support_cpp11);
    } else {
        lang = language_support(lang | language_support::support_c99);
    }
    // Why cannot I directly use istreambuf_iterator?
    auto it = lex_iterator_type(//istreambuf_iterator<char>(fin.rdbuf()), istreambuf_iterator<char>(),
                                input.begin(), input.end(),
                                lex_iterator_type::token_type::position_type(""), lang);
    while (it != lex_iterator_type()) {
        if (it->operator boost::wave::token_id() == boost::wave::token_id::T_CCOMMENT || it->operator boost::wave::token_id() == boost::wave::token_id::T_CPPCOMMENT) {
            const auto &pos = it->get_position();
            //std::cout << "Comment at " << pos.get_file() << ":" << pos.get_line() << ":" << pos.get_column() << ": " << it->get_value() << "\n";
            std::string value(it->get_value().begin(), it->get_value().end());
            auto line = pos.get_line();
            for (auto &s : split(value, '\n')) {
                size_t i;
                for (i = 0; i < s.size() && (s[i] == '/' || s[i] == '*' || isspace(s[i])); i++);
                if (i == s.size()) {
                    s = "";
                } else {
                    size_t j;
                    for (j = s.size()-1; s[j] == '/' || s[j] == '*' || isspace(s[j]); j--);
                    s = s.substr(i, j+1-i);
                }
                if (s != "") {
                    if (!ret.empty() && line == ret.back().first) {
                        ret.back().second += " ";
                        ret.back().second += std::move(s);
                    } else {
                        ret.push_back(make_pair(line, std::move(s)));
                    }
                }
                line++;
            }
        }
        it++;
    }
    return ret;
}

std::vector<std::pair<std::size_t, std::string> > file_as_comments(std::istream &fin) {
    using namespace std;
    vector<pair<size_t, string>> ret;
    size_t line_num = 0;
    for (string line; getline(fin, line);) {
        ret.push_back(make_pair(line_num, line));
        line_num++;
    }
    for (auto &line : ret) {
        size_t i;
        for (i = 0; i < line.second.size() && isspace(line.second[i]); i++);
        if (i == line.second.size()) {
            line.second = "";
        } else {
            size_t j;
            for (j = line.second.size()-1; isspace(line.second[j]); j--);
            line.second = line.second.substr(i, j+1-i);
        }
    }
    return ret;
}

void print_comments(const std::vector<std::pair<std::size_t, std::string>> &comments, std::ostream &fout) {
    fout << " /-------\n";
    for (const auto &c : comments) {
        fout << " | " << c.first << " " << c.second << "\n";
    }
    fout << " \\-------\n";
}

}
