#pragma once

#include <vector>
#include <string>

#include "config.h"

namespace gio::fbc {

std::vector<std::string> process_author_renaming(const std::string &name, const config &conf);

}
