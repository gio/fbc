#include "meta.h"

#include <iostream>

#include <boost/regex.h>

#include <nlohmann/json.hpp>

#include "names.h"

namespace gio::fbc {

std::pair<std::string, std::set<std::string>> parse_meta(const nlohmann::json &data, const config &conf) {
    using namespace std;
    using namespace nlohmann;
    set<string> authors;
    for (const auto &author : data["authors"]) {
        for (const auto &real_name : process_author_renaming(author.get<string>(), conf)) {
            authors.insert(real_name);
        }
    }
    if (data.contains("maintainers")) {
        for (const auto &maintainer : data["maintainers"]) {
            for (const auto &real_name : process_author_renaming(maintainer.get<string>(), conf)) {
                authors.insert(real_name);
            }
        }
    }
    return make_pair(data["key"].get<string>(), authors);
}

void meta_path_rec(meta_results &mr, const boost::filesystem::path &p, const config &conf) {
    using namespace std;
    using namespace boost::filesystem;
    using namespace nlohmann;
    if (is_directory(p)) {
        for (const auto &entry : directory_iterator(p)) {
            meta_path_rec(mr, entry, conf);
        }
    } else if (is_regular_file(p)) {
        if (p.filename() == "libraries.json") {
            //cerr << "Parsing " << p.string() << '\n';
            json data;
            {
                boost::filesystem::ifstream fin(p);
                fin >> data;
            }
            if (data.is_array()) {
                for (const auto &item : data) {
                    mr.authors.push_back(parse_meta(item, conf));
                }
            } else {
                mr.authors.push_back(parse_meta(data, conf));
            }
        }
    } else {
        throw runtime_error(string("unknown file type: ") + p.string());
    }
}

}
