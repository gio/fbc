#include "debian.h"

#include <string>
#include <iostream>

#include "meta.h"
#include "scanner.h"
#include "years.h"
#include "data.h"

namespace gio::fbc {

const static std::map<std::string, std::string> RENAME_MAP = {
    {"utility/enable_if", "core/enable_if"},
    {"utility/swap", "core/swap"},
    {"bind/ref", "core/ref"},
    {"math/statistical_distributions", "math/distributions"},
};

const static std::map<std::string, std::string> SUBCOMP_MAP = {
    {"in_place_factories", "in_place_factory"},
    {"value_initialized", "value_init"},
};

const static std::set<std::string> SUBDIR_COMP = {
    "spirit",
    "geometry",
    "functional",
    "algorithm",
    "numeric",
};

const static std::vector<std::pair<boost::regex, std::string>> DEREGEX_RES = {
    {boost::regex("\\.\\*", boost::regex::perl), "*"},
    {boost::regex("\\\\\\.", boost::regex::perl), "."},
    {boost::regex("^\\./", boost::regex::perl), ""},
};

const static std::set<std::string> IGNORE_IGNORE_PATH_REGEX = {
    "*/.git/*",
    "*/.git",
    "debian/*",
};

static void gen_debian_copyright_fixed(const data_t &data, std::ostream &fout) {
    using namespace std;
    fout << R"text(Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: Boost
Upstream-Contact: https://www.boost.org/
Files-Excluded:
)text";
    for (const auto &ipr : data.conf.ignore_path_regex) {
        auto ipr_str = ipr.str();
        for (const auto &deregex_re : DEREGEX_RES) {
            ipr_str = boost::regex_replace(ipr_str, deregex_re.first, deregex_re.second);
        }
        if (IGNORE_IGNORE_PATH_REGEX.find(ipr_str) != IGNORE_IGNORE_PATH_REGEX.end()) {
            continue;
        }
        fout << ' ' << ipr_str << '\n';
    }
    fout << R"text(
Files: *
Copyright: Boost project contributors
License: BSL-1.0
Comment:
 For a few files it is difficult or impossible to establish an actual
 copyright holder; most of such files carry little to no creative
 content, so the mere fact that they are covered by copyright
 protection is not obvious. Most probably all such files can safely be
 considered under the Boost Software License.

Files:
 debian/*
Copyright:
 2001 Raphael Bossek <bossekr@debian.org>
 2002-2018 Steve M. Robbins <smr@debian.org>
 2004-2008 Domenico Andreoli <cavok@debian.org>
 2013-2019 Canonical Ltd
 2018-2020 Giovanni Mascellani <gio@debian.org>
License: BSL-1.0

)text";
}

static void gen_debian_copyright_meta(const data_t &data, std::ostream &fout) {
    using namespace std;

    // Parse meta for each library
    for (const auto &item : data.mr.authors) {
        string name = item.first;
        auto it = RENAME_MAP.find(name);
        if (it != RENAME_MAP.end()) {
            name = it->second;
        }
        string files = string("libs/") + name + "/*";
        auto slash_pos = name.find('/');
        if (slash_pos != string::npos) {
            auto component = name.substr(0, slash_pos);
            if (SUBDIR_COMP.find(component) == SUBDIR_COMP.end()) {
                auto subcomp = name.substr(slash_pos+1);
                auto it2 = SUBCOMP_MAP.find(subcomp);
                if (it2 != SUBCOMP_MAP.end()) {
                    subcomp = it2->second;
                }
                files = string("libs/") + component + "/*" + subcomp + "*";
            }
        }
        fout << "Files:\n " << files << "\n";
        fout << "Copyright:\n";
        for (const auto &author : item.second) {
            fout << ' ' << author << '\n';
        }
        fout << "License: BSL-1.0\n\n";
    }
}

static void gen_debian_copyright_files(const data_t &data, std::ostream &fout) {
    using namespace std;
    using namespace boost::filesystem;
    map<pair<set<string>, set<string>>, pair<vector<path>, map<string, set<unsigned long>>>> holders;
    for (const auto &good : data.gr.good) {
        auto path = get<0>(good);
        auto fr = get<2>(good);
        if (fr.licenses.empty()) {
            continue;
        }
        if (fr.copyright.empty()) {
            continue;
        }
        set<string> names;
        std::transform(fr.copyright.begin(), fr.copyright.end(), inserter(names, names.end()), [](const auto &x) { return x.second; });
        auto &holder = holders[make_pair(names, fr.licenses)];
        holder.first.push_back(path);
        for (const auto &copy : fr.copyright) {
            holder.second[copy.second].insert(copy.first.begin(), copy.first.end());
        }
    }
    for (const auto &holder : holders) {
        fout << "Files:\n";
        for (const auto &p : holder.second.first) {
            assert(p.string().substr(0, 2) == "./");
            fout << ' ' << p.string().substr(2) << '\n';
        }
        fout << "Copyright:\n";
        for (const auto &copy : holder.second.second) {
            fout << ' '<< render_years(copy.second) << ' ' << copy.first << '\n';
        }
        fout << "License: ";
        bool first = true;
        for (const auto &lic : holder.first.second) {
            if (!first) {
                fout << " and ";
            }
            first = false;
            fout << lic;
        }
        fout << "\n\n";
    }
    for (const auto &license : data.gr.licenses) {
        fout << "License: " << license.first << '\n';
        istringstream iss(data.conf.licenses.at(license.first).front());
        for (std::string line; std::getline(iss, line); ) {
            fout << ' ' << (line != "" ? line : ".") << '\n';
        }
        fout << '\n';
    }
}

void gen_debian_copyright(const data_t &data, std::ostream &fout) {
    gen_debian_copyright_fixed(data, fout);
    gen_debian_copyright_meta(data, fout);
    gen_debian_copyright_files(data, fout);
}

}
