#pragma once

#include <boost/filesystem.hpp>

#include "meta.h"
#include "scanner.h"

namespace gio::fbc {

struct data_t {
public:
    explicit data_t(const std::string &base_dir_str, bool compute_gr = true);

    boost::filesystem::path base_dir;
    config conf;
    meta_results mr;
    global_result gr;
};

}
