#include "data.h"

namespace gio::fbc {

data_t::data_t(const std::string &base_dir_str, bool compute_gr) : base_dir(base_dir_str) {
    this->conf = read_config();
    meta_path_rec(this->mr, base_dir_str, this->conf);
    std::sort(this->mr.authors.begin(), this->mr.authors.end());
    if (compute_gr) {
        path_rec(this->gr, base_dir_str, this->conf);
        assert(this->gr.scanned.size() == this->gr.good.size() + this->gr.bad.size() + this->gr.ignored.size()
               + this->gr.useless.size() + this->gr.empty.size() + this->gr.unknown.size());
    }
}

}
