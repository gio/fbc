TEMPLATE = app
CONFIG += console c++1z
CONFIG -= app_bundle
CONFIG -= qt

LIBS += -lboost_system -lboost_filesystem -lboost_thread -lboost_wave -lboost_regex -lcryptopp -lyaml-cpp
CONFIG += link_pkgconfig
PKGCONFIG += libexif libpng

INCLUDEPATH += libs/giolib

SOURCES += \
        config.cpp \
        data.cpp \
        debian.cpp \
        license.cpp \
        main.cpp \
        meta.cpp \
        names.cpp \
        parsing.cpp \
        pictures.cpp \
        scanner.cpp \
        utils.cpp \
        years.cpp

HEADERS += \
    config.h \
    data.h \
    debian.h \
    license.h \
    meta.h \
    names.h \
    parsing.h \
    pictures.h \
    scanner.h \
    utils.h \
    years.h
